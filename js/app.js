document.addEventListener("DOMContentLoaded", () => {
  let lista = document.getElementById("lista");
  let textTarea = document.getElementById("tarea");

  // Obtiene el boton
  let boton = document.getElementById("boton");

  // Listener del click en el boton
  boton.addEventListener("click", () => {
    // Obtiene el texto digitado por el usuario
    let tarea = textTarea.value;

    // Verifica que se haya digitado texto para la tarea
    if (tarea === "") {
      alert("No se pueden crear tareas sin texto!");
      return;
    }

    // Crea un <li>
    let li = document.createElement("li");

    // Crear checkbox
    let checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    checkbox.addEventListener("click", (evento) => {
      // Obtiene el checkbox
      let elemento = evento.target;
      // Obtiene el <li> padre
      let li = elemento.parentElement;
      // Cambiar css
      li.classList.toggle("terminada");
    });

    // Crear el mensaje
    let texto = document.createTextNode(tarea);

    // Crear boton para borrar
    let botonEliminar = document.createElement("button");
    botonEliminar.textContent = "X";
    botonEliminar.addEventListener("click", (evento) => {
      // Obtiene el boton
      let elemento = evento.target;
      // Obtiene el <li> padre
      let li = elemento.parentElement;
      // Elimina el <li> padre
      li.remove();
    });

    // Se agregan los elementos al <li>
    li.appendChild(checkbox);
    li.appendChild(texto);
    li.appendChild(botonEliminar);

    // Agrega el <li> al <ul>
    lista.appendChild(li);

    // Limpiar el texto digitado
    textTarea.value = "";
  });
});
